// SPDX-License-Identifier: MIT
pragma solidity >=0.5.0 <0.9.0;

contract Owned {
    address public owner;

    constructor() payable {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(owner == msg.sender);
        _;
    }

    function changeOwner(address _owner) public onlyOwner {
        owner = _owner;
    }
}

contract Crowdsale is Owned {
    uint256 public totalSupply;
    mapping(address => uint256) public balanceOf;

    event Transfer(address indexed from, address indexed to, uint256 value);

    constructor() payable Owned() {
        totalSupply = 21000000;
        balanceOf[address(this)] = 20000000;
        balanceOf[owner] = totalSupply - balanceOf[address(this)];
    }

    receive() external payable {
        require(balanceOf[address(this)] > 0);

        uint256 tokens = (5000 * msg.value) / 1000000000000000000;
        if (tokens > balanceOf[address(this)]) {
            tokens = balanceOf[address(this)];
            uint256 valueWei = (tokens * 1000000000000000000) / 5000;
            payable(msg.sender).transfer(msg.value - valueWei);
        }

        require(tokens > 0);

        balanceOf[msg.sender] += tokens;
        balanceOf[address(this)] -= tokens;
        emit Transfer(address(this), msg.sender, tokens);
    }
}

contract SaleBresCoins is Crowdsale {
    string public standard = 'Token 1.0';
    string public name = 'BresCoin';
    string public symbol = 'BRS';
    uint8 public decimals = 0;

    constructor() payable Crowdsale() {}

    function move(address _to, uint256 _value) public {
        require(balanceOf[msg.sender] >= _value);
        balanceOf[msg.sender] -= _value;
        balanceOf[_to] += _value;
        emit Transfer(msg.sender, _to, _value);
    }
}

contract CrowdsaleContact is SaleBresCoins {
    constructor() payable SaleBresCoins() {}

    function withdraw() public onlyOwner {
        payable(owner).transfer(address(this).balance);
    }

    function kill() public onlyOwner {
        selfdestruct(payable(owner));
    }
}
