from web3 import Web3
from solcx import compile_files
from web3.middleware import geth_poa_middleware

anton = '0x0bDC4Aa1ecdB47E60edF8a6afac422416d045e02'
fedor = '0x306d014b1551d5e35b25fAD720Fe8E87508eCBAE'

class Blockchain:

    def __init__(self, default_account):
        self.w3 = Web3(Web3.HTTPProvider('http://78.47.67.222:8085', request_kwargs={'timeout': 120}))
        self.w3.eth.defaultChain = 'goerli'
        self.w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self.w3.eth.default_account = default_account

        compiled_contract = compile_files(['contracts/crowd.sol'])
        contract_id, contract_interface = compiled_contract.popitem()
        self.contract_id = contract_id
        self.contract_interface = contract_interface

    def get_balance(self, address):
        return self.w3.eth.getBalance(address)

    def send_ether(self, address_from, address_to, value):
        tx_hash = self.w3.eth.send_transaction({
            'from': address_from,
        	'to': address_to,
        	'chainId': '0x5',
        	'value': self.w3.toWei(value, 'ether'),
        	'gas': 42000
        })
        print('transaction', tx_hash)
        tx_receipt = self.w3.eth.wait_for_transaction_receipt(tx_hash)
        print('transaction is accepted', tx_receipt)

    def deploy_contract(self):
        bytecode = self.contract_interface['bin']
        abi = self.contract_interface['abi']
        contract = self.w3.eth.contract(abi=abi, bytecode=bytecode)
        tx_hash = contract.constructor().transact()
        print('contract is deployed', tx_hash)
        tx_receipt = self.w3.eth.wait_for_transaction_receipt(tx_hash)
        print('contract is accepted with address', tx_receipt.contractAddress)    

    def contract_get_owner(self, contract_address):
        abi = self.contract_interface['abi']
        contract = self.w3.eth.contract(address=contract_address, abi=abi)
        print(contract.functions.balanceOf.call())

    def contract_change_owner(self, contract_address, new_owner):
        abi = self.contract_interface['abi']
        contract = self.w3.eth.contract(address=contract_address, abi=abi)
        tx_hash = contract.functions.changeOwner(new_owner).transact({'gas': 420000, 'gasPrice': 21000})
        print('transaction was sent', tx_hash)
        tx_receipt = self.w3.eth.wait_for_transaction_receipt(tx_hash)
        print(tx_receipt)  

chain = Blockchain(default_account=anton)

print('anton balance', chain.get_balance(anton))
print('fedor balance', chain.get_balance(fedor))
# chain.send_ether(fedor, anton, 1)
#chain.deploy_contract()
contract_address = '0xfF5f9e3EF7c5d50C998abcEFDbfF547c1639c708'
print('contract balance', chain.get_balance(contract_address))
chain.send_ether(anton, contract_address, .1)
#print('contract balance', chain.get_balance(contract_address))
#print(chain.contract_say_hello(contract_address))
#print(chain.contract_get_owner(contract_address))
#print(chain.contract_change_owner(contract_address, fedor))
